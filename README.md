# Moxy
## Tech-stack
* [Moxy](https://github.com/moxy-community/Moxy) -  library that allows for hassle-free implementation of the MVP pattern in an Android Application
* [Dagger2](https://dagger.dev/) -  Dagger is a fully static, compile-time dependency injection framework for both Java and Android

![image](screen1.png)