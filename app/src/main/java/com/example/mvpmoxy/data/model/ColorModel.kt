package com.example.mvpmoxy.data.model

import android.graphics.Color

data class ColorModel(var name: String = "White", var color: Int = Color.WHITE)
