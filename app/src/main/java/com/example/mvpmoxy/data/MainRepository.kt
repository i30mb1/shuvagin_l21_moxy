package com.example.mvpmoxy.data

import android.graphics.Color
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import com.example.mvpmoxy.data.model.ColorModel
import com.example.mvpmoxy.util.extension.generateColor
import com.example.mvpmoxy.util.extension.schedule
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainRepository @Inject constructor(
    val localRepository: Zombie,
    val internetRepository: InternetRepository
) {

    var counter = 1
    fun loadColors() = Single.just(
        listOf(
            ColorModel("Black", Color.BLACK),
            ColorModel("Red", Color.RED),
            ColorModel("Gray", Color.GRAY),
            ColorModel("Yellow", Color.YELLOW),
            ColorModel("Cyan", Color.CYAN)
        )
    ).schedule(DELAY_NEW_COLOR_LIST)

    fun generateNewColor(): Single<ColorModel> {
        counter++
        val color = GENERATION_COLOR_LIMIT.generateColor()
        return Single.just(ColorModel("Color [${color.red}, ${color.green} ,${color.blue}]", color))
            .schedule(DELAY_NEW_COLOR)
    }

    companion object {
        private const val GENERATION_COLOR_LIMIT = 256
        private const val DELAY_NEW_COLOR = 1L
        private const val DELAY_NEW_COLOR_LIST = 3L
    }
}
