package com.example.mvpmoxy.di

import com.example.mvpmoxy.data.InternetRepository
import com.example.mvpmoxy.data.MainRepository
import com.example.mvpmoxy.data.Zombie
import dagger.Binds
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun getMainRepository(): MainRepository
}

@Module
abstract class ApplicationModule {

    @Binds
    abstract fun getInternetRepository(internetRepository: InternetRepository): Zombie
}
