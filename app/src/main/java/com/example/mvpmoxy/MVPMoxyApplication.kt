package com.example.mvpmoxy

import android.app.Application
import com.example.mvpmoxy.data.MainRepository
import com.example.mvpmoxy.di.DaggerApplicationComponent
import javax.inject.Inject

class MVPMoxyApplication : Application() {

    @Inject
    lateinit var mainRepository: MainRepository

    override fun onCreate() {
        super.onCreate()

        mainRepository = DaggerApplicationComponent.create().getMainRepository()
    }
}
