package com.example.mvpmoxy.util.extension

import android.graphics.Color
import kotlin.random.Random

fun Int.generateColor() =
    Color.rgb(Random.nextInt(this), Random.nextInt(this), Random.nextInt(this))
