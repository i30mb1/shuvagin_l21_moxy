package com.example.mvpmoxy.util.extension

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

fun <T> Single<T>.schedule(time: Long, timeUnit: TimeUnit = TimeUnit.SECONDS) =
    delay(time, timeUnit)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
