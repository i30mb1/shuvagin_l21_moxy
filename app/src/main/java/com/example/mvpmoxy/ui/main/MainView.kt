package com.example.mvpmoxy.ui.main

import com.example.mvpmoxy.data.model.ColorModel
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

interface MainView : MvpView {

    @StateStrategyType(SkipStrategy::class)
    fun loading(isLoading: Boolean)

    @StateStrategyType(AddToEndStrategy::class)
    fun onNewList(list: List<ColorModel>)
}
