package com.example.mvpmoxy.ui.main

import com.example.mvpmoxy.data.MainRepository
import com.example.mvpmoxy.data.model.ColorModel
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import moxy.InjectViewState
import moxy.MvpPresenter

@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {

    private val colorModelList = mutableListOf<ColorModel>()
    private val disposable = CompositeDisposable()
    lateinit var repository: MainRepository

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        initInitialColors()
    }

    fun generateColor() {
        repository.generateNewColor()
            .subscribe(object : SingleObserver<ColorModel?> {
                override fun onSuccess(colorModel: ColorModel) {
                    colorModelList.add(colorModel)
                    viewState.onNewList(colorModelList)
                    viewState.loading(false)
                }

                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                    viewState.loading(true)
                }

                override fun onError(e: Throwable) {
                    viewState.loading(false)
                }
            })
    }

    private fun initInitialColors() {
        repository.loadColors()
            .subscribe(object : SingleObserver<List<ColorModel>?> {
                override fun onSuccess(list: List<ColorModel>) {
                    colorModelList.addAll(list)
                    viewState.onNewList(colorModelList)
                    viewState.loading(false)
                }

                override fun onSubscribe(d: Disposable) {
                    disposable.add(d)
                    viewState.loading(true)
                }

                override fun onError(e: Throwable) {
                    viewState.loading(false)
                }
            })
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }
}
