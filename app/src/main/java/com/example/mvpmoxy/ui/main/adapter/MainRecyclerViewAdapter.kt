package com.example.mvpmoxy.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.mvpmoxy.data.model.ColorModel
import com.example.mvpmoxy.databinding.ItemColorBinding
import com.example.mvpmoxy.util.RecyclerViewDiffUtils

class MainRecyclerViewAdapter : RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder>() {

    var list: List<ColorModel> = emptyList()

    fun setNewList(newList: List<ColorModel>) {
        val diffUtils = RecyclerViewDiffUtils(list, newList)
        list = newList
        DiffUtil.calculateDiff(diffUtils).dispatchUpdatesTo(this)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(ItemColorBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.set(list[position])

    inner class ViewHolder(private val binding: ItemColorBinding) : RecyclerView.ViewHolder(binding.root) {

        fun set(colorModel: ColorModel) {
            binding.colorModel = colorModel
            binding.executePendingBindings()
        }
    }
}
