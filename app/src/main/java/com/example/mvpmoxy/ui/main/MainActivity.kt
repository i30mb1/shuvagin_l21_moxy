package com.example.mvpmoxy.ui.main

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvpmoxy.MVPMoxyApplication
import com.example.mvpmoxy.R
import com.example.mvpmoxy.data.InternetRepository
import com.example.mvpmoxy.data.LocalRepository
import com.example.mvpmoxy.data.MainRepository
import com.example.mvpmoxy.data.model.ColorModel
import com.example.mvpmoxy.databinding.ActivityMainBinding
import com.example.mvpmoxy.di.DaggerApplicationComponent
import com.example.mvpmoxy.ui.main.adapter.MainRecyclerViewAdapter
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class MainActivity : MvpAppCompatActivity(), MainView {

    lateinit var mainRepository: MainRepository
    @InjectPresenter
    internal lateinit var presenter: MainPresenter
    lateinit var binding: ActivityMainBinding
    private val mainAdapter by lazy { MainRecyclerViewAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

//        instead of this
        mainRepository = MainRepository(LocalRepository(), InternetRepository())
//        we can do this
        val myFactory = DaggerApplicationComponent.create()
        mainRepository = myFactory.getMainRepository()
        // or this (Singletone)
        mainRepository = (application as MVPMoxyApplication).mainRepository

        presenter.repository = mainRepository

        initRecyclerView()
        iniSwipeToRefresh()
    }

    private fun iniSwipeToRefresh() {
        binding.swipeActivityMain.setOnRefreshListener {
            presenter.generateColor()
        }
    }

    private fun initRecyclerView() {
        binding.rvActivityMain.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = mainAdapter
        }
    }

    override fun onNewList(list: List<ColorModel>) {
        mainAdapter.setNewList(list)
    }

    override fun loading(isLoading: Boolean) {
        binding.swipeActivityMain.isRefreshing = isLoading
    }
}
